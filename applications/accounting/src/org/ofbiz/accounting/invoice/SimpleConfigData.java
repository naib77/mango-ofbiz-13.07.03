package org.ofbiz.accounting.invoice;

import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;

public class SimpleConfigData {
	
	  public static String getFromPartyname(Delegator delegator) {
		  String fromPartyname = "";
		  String id="1";
		   if (id!=null && !id.isEmpty()) {
			   GenericValue cofigData = null;
			   try {
				cofigData=delegator.findOne("tb_simple_config", UtilMisc.toMap("id", id), false);
				if (cofigData==null) {
					 throw new IllegalArgumentException("The passed id [" +id + "] does not match an existing config data");
				}else {
					fromPartyname=(String) cofigData.get("fromPartyname");
				}
			} catch (GenericEntityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	        return fromPartyname;
	    }

}

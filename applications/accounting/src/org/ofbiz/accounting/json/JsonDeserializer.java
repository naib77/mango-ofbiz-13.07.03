//package org.ofbiz.accounting.json;
//
//import java.io.BufferedWriter;
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.OutputStreamWriter;
//import java.io.Writer;
//import java.nio.charset.Charset;
//import java.nio.file.Files;
//import java.nio.file.Paths;
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Statement;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import org.codehaus.jackson.JsonParseException;
//import org.codehaus.jackson.map.JsonMappingException;
//import org.codehaus.jackson.map.ObjectMapper;
//import org.codehaus.jackson.type.TypeReference;
//import org.ofbiz.accounting.invoice.InvoiceRepo;
//import org.ofbiz.accounting.invoice.InvoiceServices;
//import org.ofbiz.accounting.invoice.UserData;
//import org.ofbiz.accounting.mysql.MysqlCon;
//import org.ofbiz.base.component.ComponentConfig;
//import org.ofbiz.base.component.ComponentException;
//import org.ofbiz.service.DispatchContext;
//import org.ofbiz.service.ServiceUtil;
//
//public class JsonDeserializer {
//	public static String module = JsonDeserializer.class.getName();
//
//	public static Map<String, Object> deserializeJson(DispatchContext dctx, Map<String, Object> context) {
//
//		String path = "";
//		String logFilePath = "";
//		try {
//			path = ComponentConfig.getRootLocation("accounting") + "config/invoiceRepo.json";
//			logFilePath = ComponentConfig.getRootLocation("accounting") + "config/log.txt";
//			System.out.println("path : " + path);
//		} catch (ComponentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		try {
//			ObjectMapper mapper = new ObjectMapper();
//			/**
//			 *  THIS SECTION IS WRITTEN FOR WRITE INTO JSON FILE WITH JACKSON
//			 */
////			InvoiceRepo repo=new InvoiceRepo();
////			repo.setServerName("localhost");
////			repo.setDatabaseName("sonoo");
////			repo.setAdminUserName("root");
////			repo.setAdminPassword("Takay1#$ane");
////			
////			 mapper.writeValue(new File(path), repo);
//
//			TypeReference<InvoiceRepo> ref = new TypeReference<InvoiceRepo>() {
//			};
//			InvoiceRepo data = mapper.readValue(new File(path), ref);
//			MysqlCon.executeSelectQuery(data.getServerName(), data.getDatabaseName(), data.getAdminUserName(), data.getAdminPassword());
//		
//			//InvoiceServices.syncInvoiceByJava(dctx, context); 
//		//List<UserData> myObjects = mapper.readValue(new File(path), new TypeReference<List<UserData>>(){});
//			
//		
//			List<String> lines=Files.readAllLines(Paths.get(logFilePath), Charset.forName("UTF-8"));
//			try (Writer writer = new BufferedWriter(
//					new OutputStreamWriter(new FileOutputStream(logFilePath), "utf-8"))) {
//				for (String line : lines) {
//					if (line!=null && !line.isEmpty()) {
//						writer.write(line+"\n");
//					}
//				}
//				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
//				Date date = new Date();
//				String currentDateAndTime = dateFormat.format(date);
//				writer.write(currentDateAndTime +" : ");
//				//writer.write(data.getName()+" ");
//				//writer.write(String.valueOf(data.getVerified())+"\n");
//
//			}
//			
//			
//
//		} catch (JsonParseException e) {
//			e.printStackTrace();
//		} catch (JsonMappingException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//
//		Map<String, Object> result = ServiceUtil.returnSuccess();
//
//		return result;
//	}
//}

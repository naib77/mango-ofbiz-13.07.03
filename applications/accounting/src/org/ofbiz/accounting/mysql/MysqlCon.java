package org.ofbiz.accounting.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MysqlCon {
	
	public static void executeSelectQuery(String serverName,String databaseName,String adminUserName,String adminPassword) {
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con;
			con = DriverManager.getConnection("jdbc:mysql://"+serverName+":3306/"+databaseName,adminUserName,adminPassword);
			Statement stmt=con.createStatement();
			ResultSet rs=stmt.executeQuery("select * from emp");

			while(rs.next())
			System.out.println(rs.getInt(1)+"  "+rs.getString(2)+"  "+rs.getString(3));
			con.close();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}

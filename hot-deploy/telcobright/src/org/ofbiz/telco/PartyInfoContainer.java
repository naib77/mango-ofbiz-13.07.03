package org.ofbiz.telco;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.GenericServiceException;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceUtil;

public class PartyInfoContainer {
	public TelcobrightRepo parentRepo = null;

	public PartyInfoContainer(TelcobrightRepo parent) {
		super();
		this.parentRepo = parent;
	}

	public PartyInfoContainer() {
		// TODO Auto-generated constructor stub
	}
 
	public ArrayList<PartyInformation> externalParties = null;
	public ArrayList<PartyInformation> ofbizParties = null;

	public void UpdateParties() {
		externalParties = PopulateExternal();
		// ofbizParties=populateOfbizParties
		// find Difference
		// insert new parties to ofbiz
	}
	public static Map<String, Object> SyncExternalParties(DispatchContext dctx, Map<String, Object> context) {
		Map<String, Object> result = ServiceUtil.returnSuccess();
		ArrayList<PartyInformation> ofbizParties=populateOfbizParties(dctx, context);
		TelcobrightRepo repo = new TelcobrightRepo(new JsonDeserializer());
		repo.partyContainer=new PartyInfoContainer(repo);
		ArrayList<PartyInformation> externalParties=repo.partyContainer.PopulateExternal();
		externalParties.removeAll(ofbizParties);
		createPartyGroups(dctx, context, externalParties);
		return result;
	}
	
	
	public static Map<String, Object> sendEmailTest(DispatchContext dctx, Map<String, Object> context) {
		Map<String, Object> result = ServiceUtil.returnSuccess();
		
		return result;
	}
	
	
	
	
	public static Map<String, Object> createPartyGroups(DispatchContext dctx, Map<String, Object> context,ArrayList<PartyInformation> externalParties) {
		Map<String, Object> result = ServiceUtil.returnSuccess();
		LocalDispatcher dispatcher = dctx.getDispatcher();
		for (PartyInformation partyInformation : externalParties) {
			Map<String, Object> newContext=new HashMap<>();
			newContext.putAll(context);
			newContext.put("groupName", partyInformation.getPartnerName());
			newContext.put("preferredCurrencyUomId", "USD");
			newContext.put("externalId", partyInformation.getExternalId());
			try {
				result = dispatcher.runSync("createPartyGroup", newContext);
			} catch (GenericServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//PartyServices.createPartyGroup(dctx, newContext);
		}
		
		return result;
	}
	
	
	public  ArrayList<PartyInformation> PopulateExternal() {
		ArrayList<PartyInformation> parties = new ArrayList<PartyInformation>();
		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = this.parentRepo.conFactory.GetDBConnection();
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select * from partner");
			while (rs.next()) {
				PartyInformation pInfo = new PartyInformation(rs.getString(1), rs.getString(2));
				parties.add(pInfo);
			}
			con.close();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return parties;
	}
	
	public static ArrayList<PartyInformation> populateOfbizParties(DispatchContext dctx, Map<String, Object> context) {
		Map<String, Object> result = ServiceUtil.returnSuccess();
		Delegator delegator = dctx.getDelegator();
		LocalDispatcher dispatcher = dctx.getDispatcher();
		GenericValue userLogin = (GenericValue) context.get("userLogin");
		// GenericDelegator delegator =
		// (GenericDelegator) request.getAttribute("delegator");
		List<GenericValue> existingParties = null;
		ArrayList<PartyInformation> ofbizParties=new ArrayList<>(); 
		try {
			// The delegator.findList() method returns a Java List or null
			// if no values are found
			existingParties = delegator.findList("Party",
					EntityCondition.makeCondition("partyTypeId", EntityOperator.EQUALS, "PARTY_GROUP"), null, null,
					null, false);
			for (GenericValue genericValue : existingParties) {
				String externalId = (String) genericValue.get("externalId");
				PartyInformation pInformation=new PartyInformation(externalId, "");
				ofbizParties.add(pInformation);
				System.out.println(externalId);
			}
		} catch (GenericEntityException e) {
			return ofbizParties;
		}

		return ofbizParties;
	}

}

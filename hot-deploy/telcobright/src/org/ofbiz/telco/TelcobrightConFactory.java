package org.ofbiz.telco;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author Naib Hossain Khan
 *
 */
public class TelcobrightConFactory {
	
	
	String serverName;
	String databaseName;
	String adminUserName;
	String adminPassword;
	public TelcobrightConFactory() {
	}
	public String getServerName() {
		return serverName;
	}
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}
	public String getDatabaseName() {
		return databaseName;
	}
	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}
	public String getAdminUserName() {
		return adminUserName;
	}
	public void setAdminUserName(String adminUserName) {
		this.adminUserName = adminUserName;
	}
	public String getAdminPassword() {
		return adminPassword;
	}
	public void setAdminPassword(String adminPassword) {
		this.adminPassword = adminPassword;
	}
	public Connection GetDBConnection()
	{
		Connection con;
		try {
			con = DriverManager.getConnection("jdbc:mysql://"+serverName+":3306/"+databaseName,adminUserName,adminPassword);
			return con;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

}

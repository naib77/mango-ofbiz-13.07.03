package org.ofbiz.telco;

public class PartyInformation {
 String externalId;
 String partnerName;

public PartyInformation(String idPartner, String partnerName) {
	super();
	this.externalId = idPartner;
	this.partnerName = partnerName;
}

public PartyInformation(String externalId) {
	super();
	this.externalId = externalId;
}

public String getIdPartner() {
	return externalId;
}

public String getExternalId() {
	return externalId;
}

public void setExternalId(String externalId) {
	this.externalId = externalId;
}

public String getPartnerName() {
	return partnerName;
}

@Override
public String toString(){
	return "ID :"+this.externalId;
}
@Override
public boolean equals(Object obj){
	return (obj instanceof PartyInformation) && this.externalId.equalsIgnoreCase(((PartyInformation)obj).getExternalId());
}

}

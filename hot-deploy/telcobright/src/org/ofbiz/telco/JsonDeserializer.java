package org.ofbiz.telco;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.ofbiz.base.component.ComponentConfig;
import org.ofbiz.base.component.ComponentException;
import org.ofbiz.service.ServiceUtil;

public class JsonDeserializer {

	public TelcobrightConFactory GetTBConFactory() {
		String path = "";
		Map<String, Object> result = null;
		TelcobrightConFactory data = null;
		try {
			 path = ComponentConfig.getRootLocation("telcobright") +
			 "config/telcobrightConfig.json";
			//path = "C:/apache-ofbiz-13.07.03/hot-deploy/telcobright/config/telcobrightConfig.json";
			ObjectMapper mapper = new ObjectMapper();
			TypeReference<TelcobrightConFactory> ref = new TypeReference<TelcobrightConFactory>() {
			};
			data = mapper.readValue(new File(path), ref);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return data;
	}

}
